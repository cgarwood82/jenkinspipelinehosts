# List of Lab Hosts
This repo is going to serve as an ingress list of host to be
processed in a jenkins pipeline for patching hosts.

List is built from a `knife node list` but you can use whatever
makes sense for you.
